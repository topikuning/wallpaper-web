<?php namespace App\Composers;

use DB;
use stdClass;

class ComposerNavigation {

    public function setSidebar($view)
    {
        $categories = DB::table('categories')
                        ->orderBy('title')
                        ->get();
        
        $pages = DB::table('pages')
                     ->where('published','1')
                     ->orderBy('title')
                     ->get();

        $sub_page = [
            0  => [],
            1 => [],
            2 => [],
            3 => []
        ];


        $index = 0; 
        foreach($pages as $row){
            $sub_page[$index][] = $row;
            $index++; 
            if($index > 3) $index= 0;
        }

        $data = [
            'categories' => $categories,
            'pages' => $pages,
            'sub_page' => $sub_page
        ];
        $view->with($data);
    }

    public function getPage($view)
    {
        $pages = DB::table('pages')
                     ->where('published','1')
                     ->orderBy('title')
                     ->get();
        $view->with(['pages' => $pages]);
    }



}

