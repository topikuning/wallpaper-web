<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use DB; 
use Storage;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\GeneratePost as Gpost;

class GeneratePost extends Command {

  protected $name = 'content:scrap';

  protected $description = 'Run Scrapping data based on keywords table';


  public function __construct()
  {
    parent::__construct();
  }

  public function fire()
  {
    $this->info("run scrap command..");
    $this->gdata();
    $this->info("done..");

  }


  public function gdata(){
    $limit = 5000; 
    $start = 0; 

    $gpost = new Gpost;

    if(file_exists(storage_path() . '/app/last_scrap')){
      $start = Storage::get('last_scrap');
    }

    $data = DB::table('post_keyword')
            ->where('id', '>', $start)
            ->where('scrap', '0')
            ->take($limit)
            ->get();

    foreach($data as $row){
      $this->info('processs id '. $row->id . ' please wait');
      $gpost->generateData($row);
      $this->info('========>done<===========');

      $start = $row->id;
    }


    Storage::put('last_scrap', $start);



  }



  protected function getArguments()
  {
    return [
      //['example', InputArgument::REQUIRED, 
      //  'An example argument.'],
    ];
  }

  protected function getOptions()
  {
    return [
    ];
  }

}