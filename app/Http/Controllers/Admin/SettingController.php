<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Validator;
use stdClass;
use Session;


class SettingController extends Controller {
    private function setNavigation()
    {
        $navigation = ['parent' => 'spinner', 'child' => ''];
        $data = ['navigation' => $navigation];
        return $data;
    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        $data = $this->setNavigation();
        $record = null;
        $data = DB::table('spinner')->get();

        return view('admin.setting.index', ['data' => $data]);
    }

    public function store(Request $request){
        $spin = $request->input('spin'); 

        foreach ($spin as $key => $value) {
            DB::table('spinner')
                ->where('id',$key)
                ->update([
                    'text' => htmlentities($value),
                    'updated_at' => DB::raw('now()')
                ]);
        }

        Session::flash('message','Spinner updated'); 
        return redirect()->back();

    }

}