<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FuncCollection;
use DB;
use stdClass;
use App\Models\FormatTitle;
use Carbon\Carbon;
use App\Jobs\UploadJob;


class TagController extends Controller {


    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        $navigation = ['parent' => 'master', 'child' => 'keywords'];
        $data = ['navigation' => $navigation];
        return view('admin.tag', $data);
    }

    /**
     * read table to json
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function read(Request $request)
    {
        $draw = $request->input('draw');
        $start = $request->input('start'); 
        $length = $request->input('length'); 
        $order = $request->input('order');
        $search = $request->input('search');


        $columns = [
            'id',
            'title',
            'slug',
            'scrap',
            'created_at'
        ];


        $orderField = $columns[$order[0]['column']]; 
        $oderDirect = $order[0]['dir']; 

        $table = DB::table('post_keyword')->select('id','title','slug', 'scrap', 'created_at')->orderBy($orderField,$oderDirect); 

        if(trim($search['value'])){
            $index =0;
            foreach($columns as $column){
                if(!$index){
                    $table = $table->where($column, 'LIKE', '%'.  $search['value'] .'%');
                }else{
                    $table = $table->orWhere($column, 'LIKE', '%'.  $search['value'] .'%');
                }

                $index++;
            }
        }

        $total_count = $table->count();

        $data = $table->skip($start)->take($length)->get();
        $arr = []; 
        foreach ($data as $key => $value) {
            $tmp = [];
            foreach($value as $item){
                $tmp[] = $item;
            }
            $tmp[] = '<span class="badge bg-green edit-inline">Edit</span>'.
                     '<span class="badge bg-red delete-inline">Delete</span>';
            $arr[] = $tmp;
        }


        return [
            'draw' => $draw,
            'recordsTotal' => $total_count,
            'recordsFiltered' =>$total_count,
            'data' => $arr
        ];
    }


    /**
     * [createSlug description]
     * @param  [type] $text [description]
     * @return [type]       [description]
     */
    public function createSlug(Request $request)
    {
        $text = $request->input('text');
        if(trim($text)){
            return str_slug($text);
        }

        return "";
    }

    /**
     * [getCreate description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getCreate(Request $request)
    {
        $record = new stdClass(); 
        $record->id = "";
        $record->title = ""; 
        $record->slug = ""; 
        $record->created_at = "";
        return view('partials.tag_form',['record' => $record]);
    }

    /**
     * [postCreate description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postCreate(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required|unique:post_keyword', 
            'created_at' => 'required'
        ]);

        $data = $request->all(); 
        $data['slug'] = str_slug($data['slug']); 
        //$data['created_at'] = DB::raw('now()');

        DB::table('post_keyword')->insert($data); 
        $tag = DB::table('post_keyword')->orderBy('id','DESC')->first(); 

        return json_encode($tag);


    }

    public function postEdit(Request $request, $id){
         $this->validate($request, [
            'title' => 'required',
            'created_at' => 'required'
            //'slug' => 'required|unique:post_keyword'
        ]);

        $data = $request->all(); 
        //$data['slug'] = str_slug($data['slug']); 
        $data['updated_at'] = DB::raw('now()');  

        DB::table('post_keyword')->where('id',$id)->update($data); 

        return json_encode(DB::table('post_keyword')->where('id',$id)->first()); 

    }

    /**
     * [destroy description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function destroy($id)
    {
        $data = DB::table('post_keyword')->where('id', $id)->delete();  
        DB::table('posts')->where('post_id', $id)->delete();
        return $data;

    }

    public function getEdit($id){
        $data = DB::table('post_keyword')->where('id', $id)->first(); 
        return view('partials.tag_form', ['record' => $data]);
    }

    public function upload()
    {
        return view('admin.upload_csv');
    }

    public function postUpload(Request $request)
    {
        $this->validate($request,[
            'minutes' => 'required|integer|min:1'
        ]);
        $csv = $request->file('file');

        $file_name = 'csv_keyword_'. str_random(5); 
        $path = storage_path() . '/app'; 
        $csv->move($path, $file_name); 

        $this->dispatch(new UploadJob($path . '/'. $file_name, $request->input('minutes')));

        return ['file' => $csv->getClientOriginalName() . ' has been queued onto the job']; 

    }

}
