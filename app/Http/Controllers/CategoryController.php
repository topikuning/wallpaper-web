<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;




class CategoryController extends Controller {

    public function index($slug)
    {
        

        $keyword = DB::table('post_keyword')
                   ->where('slug', $slug)
                   ->first(); 
        if(!$keyword) abort(404);


        $posts = DB::table('posts')
                    ->where('post_id', $keyword->id)
                    ->get();
 
        if(!count($posts)){
            abort(404);
        }

        $category = "";
        $titles = "";
        $desc = "";
        $index = 0; 
        foreach($posts as $row){
            if($index < 5){
                $separator =  ", "; 
                if($index > 2) $separator = ". ";
                if($titles ==""){
                    $titles = $row->title;
                }else{
                    $titles .= $separator . $row->title;
                }

                if($index < 3) $desc = $titles;
            }

            $category = $row->keyword;
            $index++;
        }

        $now = Carbon::now();
        $tmp = []; 
        foreach($posts as $row){
            $created_at = Carbon::createFromFormat('Y-m-d H:i:s',$row->created_at); 
            if($created_at->lte($now)){
                $tmp[] = $row;
            }
        }
  
        return view('photo.pages.keyword',
            [
             'posts'=> $tmp,
             'body' => $titles,
             'category' => $category,
             'current_title' => $category,
             'current_description' =>  $category .'. '.$desc .', ' . config('site.site_title')
             ]
        );
    }


    public function byCategory($slug)
    {
        $category = DB::table('categories')
                    ->where('slug', $slug)
                    ->first();
        if(!$category) abort(404); 

        $posts = DB::table('posts')
                 ->select('posts.*')
                 ->join('post_keyword', 'post_keyword.id', '=', 'posts.post_id')
                 ->join('categories', 'categories.id', '=', 'post_keyword.category_id')
                 ->where('categories.id', $category->id)
                 ->where('posts.created_at', '<=', DB::raw('now()'))
                 ->orderBy('posts.title')
                 ->paginate(24);

        $desc = []; 
        $index = 0; 
        foreach($posts as $row){
            if($index < 6)
                $desc[] = $row->title;
            $index++;
        }

       $result = [
        'current_title' => 'Images in ' . $category->title,
        'category' => $category,
        'posts' => $posts,
        'current_menu' => 'categories',
        'current_description' => $category->title. ' - '. implode(', ', $desc),
       ];

        $page = $posts->currentPage();
        if($page > 1){
            $result['current_title'] .= ' - Page ' . $page;
            $result['noindex'] = true;
        }

      return view('photo.pages.category', $result);

    }

}