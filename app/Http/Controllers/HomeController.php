<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Libs\Superspin\SuperSpin;


class HomeController extends Controller {

    public function index()
    {
        
        $posts = DB::table('post_keyword')
                 ->select('posts.*')
                 ->join('posts', 'post_keyword.last_post', '=', 'posts.id')
                 ->where('post_keyword.created_at', '<=', DB::raw('now()'))
                 ->orderBy('post_keyword.id','DESC')
                 ->paginate(24);

        
        $result= [
                'posts' => $posts,
                'current_menu' => 'home'
        ];

        $page = $posts->currentPage();
        if($page > 1){
            $result['current_title'] = config('site.site_title'). ' - Page ' . $page;
            $result['noindex'] = true;
        }


        return view('photo.pages.home', $result);
        //return view('pages.main', $result);

    }

    public function getPopular()
    {
        $posts = DB::table('posts')
                 ->where('created_at', '<=', DB::raw('now()'))
                 ->orderBy('viewed', 'DESC')
                 ->paginate(24); 

        $desc = []; 
        $index = 0; 
        foreach($posts as $row){
            if($index < 6)
                $desc[] = $row->title;
            $index++;
        }

        $result = [
            'current_menu' => 'popular',
            'current_title' => 'Popular Images',
            'current_description' => 'Popular Images - '. implode(', ', $desc),
            'posts' => $posts
        ];

        $page = $posts->currentPage();
        if($page > 1){
            $result['current_title'] .= ' - Page ' . $page;
            $result['noindex'] = true;
        }

        return view('photo.pages.popular', $result);

    }

    public function getCategories()
    {
        $posts = DB::table('posts')
                 ->select('posts.*', 'categories.title as category', 'categories.slug as cat_slug')
                 ->join('post_keyword', 'post_keyword.last_post', '=', 'posts.id')
                 ->join('categories', 'categories.id', '=', 'post_keyword.category_id')
                 ->where('post_keyword.created_at', '<=', DB::raw('now()'))
                 ->orderBy('categories.title')
                 ->groupBy('categories.id')
                 ->get();

        $desc = []; 
        $index = 0; 
        foreach($posts as $row){
            if($index < 6)
                $desc[] = $row->title;
            $index++;
        }

       $result = [
        'current_title' => 'Images by Category',
        'posts' => $posts,
        'current_menu' => 'categories',
        'current_description' => 'Images By Category - '. implode(', ', $desc),
       ];

       return view('photo.pages.categories', $result);
    }


    public function searchData(Request $request)
    {
        $q = $request->input('q'); 
        $q = trim($q); 
        if(!$q) return $this->index(); 

        $posts = DB::table('posts')
                    ->where(function($query) use($q) {
                        $query->where('title', 'like', '%'. $q . '%')
                              ->orWhere('keyword', 'like', '%'. $q . '%');
                    })->orderBy('title') 
                    ->paginate(24); 

        $page = ' page '. $posts->currentPage();


        return view('photo.pages.search',[
                'posts' => $posts,
                'search' => $q,
                'current_title' => 'Search ' . $q . $page,
                'no_description' => true
            ]);



    }


    public function getSingle($slug)
    {
        $code = explode('-', $slug);
        $post = DB::table('posts')
                ->where('created_at', '<=', DB::raw('now()'))
                ->where('code', $code[0])
                ->first();
        if(!$post) abort(404); 
        $post->created_at = Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at);
        //if slow dont update the db 
        $statement = 'UPDATE posts SET viewed = viewed +1 WHERE id = '. $post->id . ' LIMIT 1'; 
        DB::statement($statement);

        $post_random = explode(',', $post->post_random);

       $details = [];
       if($post->post_random){
         $query = 'SElECT * FROM posts WHERE id IN('.$post->post_random.') ORDER BY FIELD(id, '.$post->post_random.')';
         $details = DB::select($query);
       }
        $body = [];
        $desc = [];

        foreach ($details as $key => $value) {
            if($key < 4){
                $body[] = $value->title;
            } 
            if($key < 3){
                $desc = $body;
            }       
        }

        $now = Carbon::now();
        $tmp = []; 
        foreach($details as $row){
            $created_at = Carbon::createFromFormat('Y-m-d H:i:s',$row->created_at); 
            if($created_at->lte($now)){
                $tmp[] = $row;
            }
        }

 
        return view('photo.pages.single',
            ['post' => $post,
              'body' => $body,
             'details' => $tmp,
             'current_title' => $post->title .', '. $post->keyword,
             'current_description' => implode('. ', $desc) .'. '. config('site.site_title')  
            ]);
    }


    public function getPage($slug)
    {
        $data = DB::table('pages')
                    ->where('published','1')
                    ->where('slug', $slug)
                    ->first(); 

        if(!$data) return abort(404);

        $popular = DB::table('posts')
                   ->where('created_at', '<=', DB::raw('now()'))
                   ->orderBy('viewed', 'DESC')
                   ->take(3)
                   ->get();

        $recent = DB::table('posts')
                   ->where('created_at', '<=', DB::raw('now()'))
                   ->orderBy('created_at', 'DESC')
                   ->take(3)
                   ->get();

        $cat_photo = DB::table('categories')
                     ->orderBy('title')
                     ->get();



        return view('photo.pages.page',
            ['page' => $data,
             'current_title' =>$data->title,
             'noindex' => true,
             'current_menu' => 'pagesx',
             'popular' => $popular,
             'recent' => $recent,
             'cat_photo' => $cat_photo,
             'current_description' => str_limit($data->body,50)
            ]);
        
    }
}