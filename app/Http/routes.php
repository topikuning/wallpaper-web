<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$app->group(['prefix' => 'badmin', 'middleware' => 'auth.admin', 'namespace' => 'App\Http\Controllers\Admin'], function() use ($app){
    $app->get('/createSlug', 'TagController@createSlug');
    $app->get('/','DashBoardController@index');
    
    $app->get('/pages','PageController@index');
    $app->get('/pages/create','PageController@create');
    $app->post('/pages/create','PageController@store');
    $app->get('/pages/read', 'PageController@read');
    $app->get('/pages/edit/{id}', 'PageController@edit');
    $app->delete('/pages/{id}', 'PageController@destroy');
    $app->post('/pages/attribute','PageController@postEditAtrribute');

    $app->get('/spinner','SettingController@index'); 
    $app->post('/spinner','SettingController@store');

    $app->get('/profile','UserController@getProfile'); 
    $app->post('/profile','UserController@postProfile'); 

    $app->get('/keywords','TagController@index');
    $app->get('/keywords/read','TagController@read');
    $app->get('/keywords/create','TagController@getCreate');
    $app->post('/keywords/create','TagController@postCreate');
    $app->delete('/keywords/{id}', 'TagController@destroy');
    $app->get('/keywords/edit/{id}', 'TagController@getEdit');
    $app->post('/keywords/edit/{id}', 'TagController@postEdit');
    $app->get('/keywords/upload', 'TagController@upload');
    $app->post('/keywords/upload', 'TagController@postUpload');

});



$app->group(['namespace' => 'App\Http\Controllers'], function() use ($app) {
    $app->get('/', 'HomeController@index');
    $app->get('/popular.html', 'HomeController@getPopular');
    $app->get('/categories.html', 'HomeController@getCategories');
    $app->get('/category/{slug}.html', 'CategoryController@byCategory');


    $app->get('/auth/login', 'AuthController@getLogin');
    $app->post('/auth/login', 'AuthController@postLogin');
    $app->get('/auth/logout', 'AuthController@getLogout');
    $app->get('auth/register', 'AuthController@getRegister');
    $app->post('auth/register', 'AuthController@postRegister');

    $app->get('/rss','RssController@index');
    $app->get('/page/{slug}.html', 'HomeController@getPage');


    $app->get('/sitemap.xml','SiteMapController@index');
   
    $app->get('/sitemap/sitemap-{code}.xml','SiteMapController@posts');

    /**
    $app->get('/sitemap-img.xml','SiteMapController@image');
    $app->get('/sitemap/image-{page}.xml','SiteMapController@images');
    **/

    $app->get('/search.html','HomeController@searchData');
    

    $app->get('/{slug}.html', 'HomeController@getSingle');
    $app->get('/tag/{keyword}.html','CategoryController@index');

        
    $app->get('/assets/images/{slug}','ImageController@getImage');
    $app->get('/assets/fullimage/{slug}','ImageController@getFullImage');
    $app->get('/download/{width}/{height}/{slug}', 'ImageController@downloadImage');
});
