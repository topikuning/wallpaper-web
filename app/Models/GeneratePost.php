<?php 
namespace App\Models;

use DB; 
use Carbon\Carbon; 
use App\Models\FormatTitle;


class GeneratePost {

    protected static $curlDataWritten = 0;
    protected static $curlFH = false;
    
	public function __construct()
	{
		$this->formatTitle = new FormatTitle;
	}

	public function runScrap($keyword)
	{
		$engine = config('site.engine_scrap');
		$url = config('site.node_server_url') . '/output.json?site='. $engine .'&keyword='. urlencode($keyword);

	      $ch = curl_init($url);
	      curl_setopt_array(
	        $ch,
	        array(
	           CURLOPT_SSL_VERIFYPEER => false,
	           CURLOPT_RETURNTRANSFER  => true
	        )
	      );
	      $content = curl_exec($ch);
	      curl_close($ch);
	      if($content){
	        $data = json_decode($content); 
	        $arr = [];
	        $keyword = $this->formatTitle->safe_string_insert($keyword, 'title');
	        foreach($data as $row){
	          if($row->title){
	              $en_title = $this->formatTitle->safe_string_insert($row->title, 'title');
	              $en_body = $this->formatTitle->safe_string_insert($row->body, 'desc');
	              if(isset($row->thumb_url))
	              $arr[] =[
	                'title' => $en_title,
	                'body' => $en_body,
	                'keyword' => $keyword,
	                'slug_keyword' => str_slug($keyword),
	                'thumb_url' => $row->thumb_url,
	                'url' =>$row->image->surl,
	                'image_url' => $row->image->imgurl
	              ];
	          }

	        }

	        return $arr;
	      }else{
	        return false;
	      }

	}

	public function generateData($keyword)
	{
		$data = $this->runScrap($keyword->title); 
		$data_array = [];
	    $limit = 24;
	    $index = 0;
	    $key_date = Carbon::createFromFormat('Y-m-d H:i:s', $keyword->created_at);
		foreach($data as $row){
			$row['slug'] = str_slug($row['title']);
			if(!DB::table('posts')->where('slug','like',$row['slug'])->first()){
	            $contentType = $this->getResponseImage($row['image_url']);
	            $arr = explode('/', $contentType); 
	            if(count($arr)==1) $contentType = false;
	            	if($contentType){
						if($index < $limit){
		                    $ratingCount = rand(0,1000);
		                    $ratingValue = rand(0,100); 
		                    $row['code'] = $this->createUniquRandomKey('posts');
		                    $row['local_image'] = '1'; 
		                    $row['post_id'] = $keyword->id;
		                    $row['ratingCount'] = $ratingCount;
		                    $row['ratingValue'] = $ratingValue;
		                    $row['created_at'] = $key_date->addMinutes(10)->format('Y-m-d H:i:s');
		                    $id = DB::table('posts')->insertGetId($row);
		                    $row['id'] = $id;
		                    $data_array[] = $row;
						}
						$index++;
					}
			}
		}

		if(count($data_array)){
			$first = $data_array[0]; 
      		$rand = $data_array; 
      		shuffle($rand); 
      		$last = $rand[0]['id'];
			DB::table('post_keyword')->where('id', $keyword->id)->update(['last_post' => $last, 'scrap' => '1' ]);
			$post_id = $keyword->id; 

			DB::table('posts')
				->where('id', $last)
				->update(['created_at' => $keyword->created_at]); 


	        $ids = []; 
	        foreach ($data_array as $key => $value) {
	          $ids[] = $value['id'];
	        }


	        foreach ($data_array as $key => $value) {
	          if($key > 0){
	            shuffle($ids);
	          }
	          DB::table('posts')
	              ->where('id',$value['id'])
	              ->update(['post_random' => implode(',',$ids)]);
	        }
		}



	}

  private function createUniquRandomKey($table)
  {
    $newslug = strtolower(str_random(16));
    do {
      $exist = DB::table($table)->where('code','like',$newslug)->first();
      if($exist){
        $newslug = str_random(16);
      }
    } while ($exist);

    return $newslug;
  }

  private function getResponseImage($url)
  {
      // Assume failure.
      $result = -1;
      $success = false;
      $userAgent = 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0';


      $curl = curl_init( $url );

      // Issue a HEAD request and follow any redirects.
      curl_setopt( $curl, CURLOPT_NOBODY, true );
      curl_setopt( $curl, CURLOPT_HEADER, true );
      curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,15);
      curl_setopt($curl, CURLOPT_TIMEOUT, 10);
      curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
      curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
      curl_setopt( $curl, CURLOPT_USERAGENT, $userAgent );
      curl_setopt( $curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
      $data = curl_exec( $curl );
      $contentType = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);
      curl_close( $curl );

      if( $data ) {
        $content_length = "unknown";
        $status = "unknown";

        if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches ) ) {
          $status = (int)$matches[1];
        }

        if( preg_match( "/Content-Length: (\d+)/", $data, $matches ) ) {
          //$content_length = (int)$matches[1];
        }

        // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
        if( $status == 200 || ($status > 300 && $status <= 308) ) {
          if( preg_match( "/Content-Type: (\bimage\b)/", $data, $matches ) ) {
              $success = $contentType;
          }

        }
      }

      return $success;
  }



}