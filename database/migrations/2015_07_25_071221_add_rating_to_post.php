<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRatingToPost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->integer('ratingCount')->unsigned()->index();
            $table->integer('ratingValue')->unsigned()->index();
            $table->integer('viewed')->unsigned()->index(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('ratingCount');
            $table->dropColumn('ratingValue');
            $table->dropColumn('viewed');
        });
    }
}
