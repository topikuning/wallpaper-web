<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScrapToPostKeyword extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('post_keyword', function (Blueprint $table) {
            $table->boolean('scrap')->index();
            $table->integer('category_id')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post_keyword', function (Blueprint $table) {
            $table->dropColumn('scrap');
            $table->dropColumn('category_id');
        });
    }
}
