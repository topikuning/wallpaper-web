<?php 

return [
    'site_url' => 'http://www.nama.situs/',
    'site_title' => '-',
    'site_desc' => '---',
    'site_footer' => 'Copyright 2015',
    'fb_link' => '#',
    'tw_link' => '#',
    'gplus_link' => '#',
    'node_server_url' => 'http://localhost:4000',
    'keywords_per_scrap' => '500',
    'engine_scrap' => 'bing', //bing or yandex
    'run_scrap_at' => '23:00',
    'auto_post_twit' => false, 
    'has_tag'  => ['#home','#kitchen','#furniture','#design','#interiordesign','#decor'],
    'add_word' => ['Cool ', 'Awesome ', 'Top ', 'Best '],
    'local_image' => false,
];
