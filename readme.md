--- Auto wallpaper site 
--required php 5.5.9 ~

$ cd /rootapp

1. copy/rename .env.example di root folder menjadi .env
   set writeable storage IMPORTANT
   $ chmod -R o+w storage/  
2. buka .env sesuaikan database, set jadi production mode, debug mode false
3. buka config/site.php untuk setting nama site, deksripsi, footer, dll
   site_url (wajib) berpengaruh ke sitemap

3. $ composer install 
4. $ php artisan migrate

--scrapping data (wajib install)
5. install latest nodejs dari https://nodejs.org/
   - jika memakai centos5 baca file install_nodejs_on_centos5
   setelah install
   $ cd nodescrap 
   $ sudo npm install -g forever
   $ forever start app.js
   - tambahkan forever sebagai service saat boot up server
     forever start /path/to/appweb/nodescrap app.js
   
6. install supervisor
sudo apt-get install supervisor
7. edit file wallpaper.conf (line 1, 3, 9) dan simpan di /etc/supervisor/conf.d/

sudo supervisorctl reread

sudo supervisorctl update

sudo supervisorctl start wallpaper-worker:*

readme detail http://lumen.laravel.com/docs/queues