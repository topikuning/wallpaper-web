<?xml version="1.0" encoding="UTF-8"?>
 <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">
 @foreach($posts as $row)
 <url>
   <loc>{{ url($row->code .'-'. $row->slug .'.html') }}</loc>
   <?php $ext = pathinfo($row->image_url, PATHINFO_EXTENSION); ?>
   <image:image>
     <image:loc>{{ url('assets/fullimage/'. $row->slug . '-'. $row->code . '.' .$ext) }}</image:loc>
   </image:image>
 </url> 
 @endforeach
</urlset> 