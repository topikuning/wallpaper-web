    <script type="text/javascript" src="{{ url('canvas_theme/js/jquery.min.js') }}"></script> 
    <script type="text/javascript" src="{{ url('canvas_theme/js/jquery.isotope.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('canvas_theme/js/jquery-ui.min.js') }}"></script>    
    <script type="text/javascript" src="{{ url('canvas_theme/js/modules.js') }}"></script>
    <script type="text/javascript" src="{{ url('canvas_theme/js/jquery.nivo.slider.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('canvas_theme/js/theme.js') }}"></script> 
    <script type="text/javascript" src="{{ url('canvas_theme/js/sorting.js') }}"></script>  