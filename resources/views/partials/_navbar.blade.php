        <header>
            <div class="container fw">
                <div class="row">
                    <div class="fw_ip">
                        <div class="span12 logo_links_cont_hor">
                            <div class="fl noselect">
                                <a href="{{ url('/') }}" class="logo"><img src="/canvas_theme/img/logo.png" alt="{{ config('site.site_title') }}" class="non_retina_image"><img src="/canvas_theme/img/retina/logo.png" alt="{{ config('site.site_title') }}" class="retina_image"></a>
                            </div>
                            <div class="fr">
                                <div class="horizontal_menu">
                                    <div class="menu-main-container">
                                      <ul class="menu">
                                            <li><a href="{{ url('/') }}">Home</a></li>
                                            @foreach($pages as $page)
                                            <li><a href="{{ url('page/'. $page->slug) }}.html"><span>{{ $page->title }}<i class="icon-chevron-down"></i></span></a></li>
                                            @endforeach
                                      </ul>
                                  </div>
                                </div>                                    
        

                                <div class="phone_menu">
                                    <span class="menu_toggler"></span>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>                    
            <div class="gt3_menu">
                <div class="search_form_block">
                    <form name="search_form" method="get" action="/search" class="search_form">
                        <input type="text" name="q" placeholder="Search" value="" title="Search" class="field_search">
                        <input type="submit" name="submit_search" value="Search" title="" class="s_btn_search">
                        <i class="icon-search"></i>
                        <div class="clear"></div>
                    </form>
                </div>
            <div class="menu_scroll">
                    <div class="menu-main-container"></div>
              </div>
            </div>      
        </header>