    <footer>
        <div class="ip">
            <div class="fl">
                <div>{{ config('site.site_footer') }}</div>
            </div>
            <div class="fr">
                <div class="socials">
                    <span class="follow_text active">Follow</span>
                    <span class="sh_fo_detail inline_block">
                        <a class="stand_icon soc-facebook-sign" target="_blank" href="http://facebook.com" title="Facebook"></a>
                        <a class="stand_icon soc-twitter" target="_blank" href="http://twitter.com" title="Twitter"></a>
                        <a class="stand_icon soc-instagram" target="_blank" href="http://instagram.com" title="Instagram"></a>
                  </span>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </footer>  