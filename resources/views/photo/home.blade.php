
<!doctype html>
<html>
@include('photo.partials._header')
<body class="theme-style-1">
<div id="cp-wrapper"> 
  @include('photo.partials._sub_header')
  @yield('content')
  @include('photo.partials._footer')
</div>
<!--jQuery 1.11.3--> 
<script src="/photo_store/js/jquery-1.11.3.min.js"></script> 
<!--bootstrap 3.3.5--> 
<script src="/photo_store/js/bootstrap.min.js"></script> 
<!--OWL CAROUSEL JS--> 
<script src="/photo_store/js/owl.carousel.min.js"></script> 
<!--Background Moving Start--> 
<script src='/photo_store/js/bg-moving.js'></script> 
<!-- isotope --> 
<script src="/photo_store/js/isotope.pkgd.min.js"></script> 
<!--SCROLL FOR SIDEBAR NAVIGATION--> 
<script src="/photo_store/js/jquery.mCustomScrollbar.concat.min.js"></script> 
<!--CUSTOM SCRIPT--> 
<script src="/photo_store/js/custom.js"></script> 
<!--JQUERY END-->
</body>
</html>