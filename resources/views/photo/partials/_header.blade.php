<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  @if(!isset($current_title))
      <title>{{ config('site.site_title') }}</title>
  @else
      <title>{{ $current_title }} - {{ config('site.site_title') }}</title>
  @endif
  @if(!isset($no_description))
  @if(!isset($current_description))
  <meta name="description" content="{{ config('site.site_desc') }}">
  @else

  <meta name="description" content="{{ $current_description }}">
  @endif
  @endif

  @if(isset($noindex))
      <meta name="robots" content="noindex,follow" />
  @endif

  <!--CUSTOM CSS-->
  <link href="/photo_store/css/custom.css" rel="stylesheet" type="text/css">
  <!--COLOR CSS-->
  <link href="/photo_store/css/color.css" rel="stylesheet" type="text/css">
  <!--bootstrap 3.3.5-->
  <link href="/photo_store/css/bootstrap.css" rel="stylesheet" type="text/css">
  <!--RESPONSIVE CSS-->
  <link href="/photo_store/css/responsive.css" rel="stylesheet" type="text/css">
  <!--FONTAWESOME CSS-->
  <link href="/photo_store/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!--OWL CAROUSEL CSS-->
  <link href="/photo_store/css/owl.carousel.css" rel="stylesheet" type="text/css">
  <!--ICONMOON CSS-->
  <link href="/photo_store/css/iconmoon.css" rel="stylesheet" type="text/css">
  <!--SCROLL FOR SIDEBAR NAVIGATION-->
  <link href="/photo_store/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
  <!--YAMM MENU CSS-->
  <link href="/photo_store/css/yamm-menu.css" rel="stylesheet" type="text/css">
  <!--FAVICON ICON-->
  <link rel="icon" href="/photo_store/images/favicon.ico" type="image/x-icon">
  <!--GOOGLE FONTS-->
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700,900' rel='stylesheet' type='text/css'>
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
  <script type="text/javascript">
      window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":"/page/privacy-policy.html","theme":"dark-bottom"};
  </script>

  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
  <!-- End Cookie Consent plugin -->
  </head>