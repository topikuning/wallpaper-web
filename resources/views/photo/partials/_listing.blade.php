                     	@foreach($listing as $row)
                     	<?php $ext = pathinfo($row->image_url, PATHINFO_EXTENSION); ?>
                        <div class="item"><a href="{{ url($row->code.'-'.$row->slug) }}.html" title="{{ $post->title }}"><img src="{{ url('assets/images/'. $row->slug . '-thumb.' .$ext) }}" alt="{{ $post->title }}"></a></div>
                        @endforeach