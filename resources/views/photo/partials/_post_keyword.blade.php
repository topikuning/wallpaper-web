          @foreach($posts as $row)
          <?php $ext = pathinfo($row->image_url, PATHINFO_EXTENSION); ?>
          <li class="item">
            <div class="cp-box"><img src="{{ url('assets/images/'. $row->slug . '-thumb.' .$ext) }}" alt="{{ $row->title }}">
              <div class="cp-text-box">
                <h2><a href="{{ url($row->code . '-'.  $row->slug . '.html') }}" title="{{ $row->title }}">{{ str_limit($row->title,30) }}</a></h2>
                <strong class="title"><a href="{{ url('/tag/'.$row->slug_keyword . '.html') }}">{{ $row->keyword }}</a></strong>
                <div class="detail-row">
                  <ul>
                    <li><a href="#"><i class="fa fa-eye"></i>{{ $row->viewed }}</a></li>
                    <li><a href="#"><i class="fa fa-heart"></i>{{ $row->ratingCount }}</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </li>
          @endforeach