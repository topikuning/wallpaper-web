  <!--FOOTER START-->
  <footer id="footer">
    <div class="footer-social">
      <div class="cp-sidebar-social"> <strong class="title">Connect us on</strong>
        <ul>
          <li><a href="#"><span class="icon-socialicons-01"></span></a></li>
          <li><a href="#"><span class="icon-socialicons-02"></span></a></li>
          <li><a href="#"><span class="icon-socialicons-03"></span></a></li>
          <li><a href="#"><span class="icon-socialicons-04"></span></a></li>
          <li><a href="#"><span class="icon-socialicons-05"></span></a></li>
        </ul>
      </div>
    </div>
    <div class="copyrights"><a href="/"><strong class="copy">{{ config('site.site_footer')}} </strong></a></div>
  </footer>
  <!--FOOTER END--> 