  <!--HEADER START-->
  <header id="header">
    <section class="logo-row"> 
      <!--BURGER NAVIGATION SECTION START-->
      <div class="sidebar-menu-box">
        <div class="cp-burger-nav"> 
          <!--BURGER NAV BUTTON-->
          <div id="cp_side-menu-btn" class="cp_side-menu"><a href="#" class=""><span class="icon-icons-07"></span></a></div>
          <!--BURGER NAV BUTTON--> 
          
          <!--SIDEBAR MENU START-->
          <div id="cp_side-menu"> <a href="#" id="cp-close-btn" class="crose"><i class="fa fa-close"></i></a>
            <div class="cp-top-bar">
              <div class="login-section">
              </div>
            </div>
            <strong class="logo-2"><a href="/">{{ config('site.site_title') }}</a></strong>
            <form action="/search.html" method="get">
              <input type="text" placeholder="Search photos" required name="q">
              <button><span class="icon-icons-06"></span></button>
              <em>... or choose a category on right.</em>
            </form>
            <div class="content mCustomScrollbar">
              <div id="content-1" class="content">
                <div class="cp_side-navigation">
                  <nav>
                    <ul class="navbar-nav">
                      <li class="active"><a href="/">Fresh</a></li>
                      <li><a href="/popular.html">Popular</a></li>
                      <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Categories<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                          @foreach($categories as $row)
                          <li> <a href="{{ url('category/'. $row->slug . '.html') }}">{{ $row->title }}</a> </li>
                          @endforeach
                        </ul>
                      </li>
                      <li class="dropdown"><a href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Pages<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                          @foreach($pages as $row)
                          <li> <a href="{{ url('page/'. $row->slug . '.html') }}">{{ $row->title }}</a> </li>
                          @endforeach
                        </ul>
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
            <div class="cp-sidebar-social"> <strong class="title">Connect us on</strong>
              <ul>
                <li><a href="#"><span class="icon-socialicons-01"></span></a></li>
                <li><a href="#"><span class="icon-socialicons-02"></span></a></li>
                <li><a href="#"><span class="icon-socialicons-03"></span></a></li>
                <li><a href="#"><span class="icon-socialicons-04"></span></a></li>
                <li><a href="#"><span class="icon-socialicons-05"></span></a></li>
              </ul>
            </div>
            <strong class="copy">{{ config('site.site_footer') }} </strong> </div>
          <!--SIDEBAR MENU END--> 
          
        </div>
      </div>
      <!--BURGER NAVIGATION SECTION END-->
      <div class="login-section">
      </div>
      <div class="container"> <strong class="logo"><a href="/">{{ config('site.site_title') }}</a></strong> </div>
    </section>
    <!--NAVIGATION ROW START-->
    <section class="cp-navigation-row">
      <div class="container">
        <div class="navbar yamm navbar-default">
          <div class="navbar-header">
            <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          </div>
          <div id="navbar-collapse-1" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <?php $current_menu = isset($current_menu)? $current_menu:""; ?> 
              <li class="{{ ($current_menu=='home')?'active':'' }}"><a href="/">Fresh</a></li>
              <li class="{{ ($current_menu=='popular')?'active':'' }}"><a href="/popular.html">Popular</a></li>
              <li class="{{ ($current_menu=='categories')?'active':'' }}"><a href="/categories.html">Categories</a></li>
              <!-- Classic list -->
              <li class="dropdown {{ ($current_menu=='pages')?'active':'' }}"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Pages<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li>
                    <div class="yamm-content">
                      <div class="row">
                        <ul class="col-sm-3 list-unstyled">
                          @foreach($sub_page[0] as $row)
                          <li> <a href="{{ url('page/'. $row->slug . '.html') }}">{{ $row->title }}</a> </li>
                          @endforeach
                        </ul>
                        <ul class="col-sm-3 list-unstyled">
                          @foreach($sub_page[1] as $row)
                          <li> <a href="{{ url('page/'. $row->slug . '.html') }}">{{ $row->title }}</a> </li>
                          @endforeach
                        </ul>
                        <ul class="col-sm-3 list-unstyled">
                          @foreach($sub_page[2] as $row)
                          <li> <a href="{{ url('page/'. $row->slug . '.html') }}">{{ $row->title }}</a> </li>
                          @endforeach
                        </ul>
                        <ul class="col-sm-3 list-unstyled">
                          @foreach($sub_page[0] as $row)
                          <li> <a href="{{ url('page/'. $row->slug . '.html') }}">{{ $row->title }}</a> </li>
                          @endforeach
                        </ul>
                      </div>
                    </div>
                  </li>
                </ul>
              </li>
              <li>
                <div class="cp-search-box"><a href="#" id="searchtoggl"><span class="icon-icons-06"></span></a></div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!--NAVIGATION ROW END--> 
  </header>
  <!--HEADER END--> 

  <!--SEACRCH FORM START-->
  <div class="cp-search-outer"> <strong class="title">Search Images Vectors And More...</strong>
    <div id="searchbar">
      <form id="searchform" action="/search.html" method="get">
        <input placeholder="Type your text here" type="text" required name="q">
        <button type="submit" value=""><i class="fa fa-search"></i></button>
      </form>
    </div>
  </div>
  <!--SEACRCH FORM END--> 