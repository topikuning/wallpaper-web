@extends('photo.home')

@section('content')
  <!--Inner Header Start-->
  <section class="cp-inner-banner">
    <h1>Images By Category</h1>
    <ol class="breadcrumb">
      <li class="active"><a href="/">Home</a></li>
      <li class="active">Categories</li>
    </ol>
  </section>
  <!--Inner Header End--> 
  
  <!--main Content Start-->
  <div class="cp-main-content top-50 cp-category">
    <div class="cp-gallery-fluid">
      <div class="cp-gallery-masonry-1 gallery-grid gallery">
        <ul class="cp-grid isotope items">
          @foreach($posts as $row)
          <?php $ext = pathinfo($row->image_url, PATHINFO_EXTENSION); ?>
          <li class="item">
            <div class="cp-box"><img src="{{ url('assets/images/'. $row->slug . '-thumb.' .$ext) }}" alt="{{ $row->category }}">
              <div class="cp-text-box">
                <h2><a href="{{ url('category/'. $row->cat_slug .'.html') }}" title="{{ $row->category }}">{{ $row->category }}</a></h2>
                <div class="detail-row">
                  <ul>
                    <li><a href="#"><i class="fa fa-eye"></i>{{ $row->viewed }}</a></li>
                    <li><a href="#"><i class="fa fa-heart"></i>{{ $row->ratingCount }}</a></li>
                  </ul>
                </div>            
              </div>
            </div>
          </li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
  <!--main Content End--> 

@endsection