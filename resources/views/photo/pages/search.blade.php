@extends('photo.home')

@section('content')
  <!--Inner Header Start-->
  <section class="cp-inner-banner">
    <h1>Search Images in terms {{ $search }}</h1>
    <ol class="breadcrumb">
      <li class=""><a href="/">Home</a></li>
      <li class="active">Search</li>
    </ol>
  </section>
  <!--Inner Header End--> 
  
  <!--main Content Start-->
  <div class="cp-main-content top-50 cp-category">
    <div class="cp-gallery-fluid">
      <div class="cp-gallery-masonry-1 gallery-grid gallery">
        <ul class="cp-grid isotope items">
          @include('photo.partials._post_keyword')
        </ul>
      </div>
      <div class="cp-pagination">
        <ul class="pagination">
          <li> <a aria-label="Previous" href="{{ str_replace('/?', '?q='.urlencode($search).'&', $posts->previousPageUrl()) }}" title="Previous Page"> <i class="fa fa-angle-left"></i> </a> </li>
          <li> <a aria-label="Next" href="{{ str_replace('/?', '?q='.urlencode($search).'&', $posts->nextPageUrl()) }}" title="Next Page"> <i class="fa fa-angle-right"></i> </a> </li>
        </ul>
      </div>
    </div>
  </div>
  <!--main Content End--> 

@endsection