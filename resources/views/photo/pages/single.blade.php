@extends('photo.home')

@section('content')
<?php $ext = pathinfo($post->image_url, PATHINFO_EXTENSION); ?> 
<?php $url = url('assets/fullimage/'. $post->slug . '-'. $post->code . '.' .$ext); ?>
 <meta property="og:type" content="article" />
 <meta property="og:title" content="{{ $current_title }} - {{ config('site.site_title') }}" />
 <meta property="og:url" content="{{ Request::url() }}"/>
 <meta property="og:site_name" content="{{ config('site.site_title') }}" />
 <meta property="og:description" content="{{ implode('. ', $body) }}"/>
 <meta property="og:image" content="{{ $url }}" />
<section class="cp-single-photo">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-5">
            <div class="frame"><a href="{{ url('assets/fullimage/'. $post->slug . '-'. $post->code . '.' .$ext) }}"><img src="{{  url('assets/fullimage/'. $post->slug . '-'. $post->code . '.' .$ext) }}" alt="{{ $post->title }}"></a>
            <!-- IKLAN -->
            </div>
         </div>
         <div class="col-md-7">
            <div class="cp-text-box">
               <div class="row">
                  <div class="col-md-8">
                     <div class="left-col">
                        <h1><a href="#">{{ $post->title }} </a></h1>
                        <div class="detail-row">
                           <ul>
                              <li><a href="#"><i class="fa fa-eye"></i>{{ $post->viewed }}</a></li>
                              <li><a href="#"><i class="fa fa-heart"></i>{{ $post->ratingCount }}</a></li>
                              <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::url()) }}" target="_blank"><i class="fa fa-share-alt"></i></a></li>
                           </ul>
                           <!-- IKLAN -->
                        </div>
                        <p><a href="/">{{ $post->keyword }}, </a>{{ implode('. ', $body) }}</p>
                        <div class="cp-price-box">
                           <form>
                              <ul>
                                 <li>
                                    <label for="id1">Iphone/Android</label>
                                    <b><a href="{{ url('download/320/480/'. $post->slug) }}"  rel="nofollow">320x480</a> / <a href="{{ url('download/640/960/'. $post->slug) }}"  rel="nofollow">640x960</a> / <a href="{{ url('download/640/1136/'. $post->slug) }}"  rel="nofollow">640x1136</a></b> 
                                 </li>
                                 <li>
                                    <label for="id2">Ipad/Tablet</label>
                                    <b><a href="{{ url('download/1024/1024/'. $post->slug) }}"  rel="nofollow">1024x1024</a></b> 
                                 </li>
                                 <li>
    
                                    <label for="id3">Desktop</label>
                                    <b><a href="{{ url('download/1024/758/'. $post->slug) }}"  rel="nofollow">1024x768</a> / <a href="{{ url('download/1152/864/'. $post->slug) }}"  rel="nofollow">1152x864</a> / <a href="{{ url('download/1280/960/'. $post->slug) }}"  rel="nofollow">1280x960</a> / <a href="{{ url('download/1600/1200/'. $post->slug) }}"   rel="nofollow">1600x1200</a></b> 
                                 </li>
                                 <li>

                                    <label for="id4">HD</label>
                                    <b><a href="{{ url('download/1280/720/'. $post->slug) }}"  rel="nofollow">1280x720</a> / <a href="{{ url('download/1366/768/'. $post->slug) }}"  rel="nofollow">1366x768</a> / <a href="{{ url('download/1600/900/'. $post->slug) }}"  rel="nofollow">1600x900</a> / <a href="{{ url('download/1920/1080/'. $post->slug) }}"  rel="nofollow">1920x1080</a></b> 
                                 </li>
                              </ul>
                           </form>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="cp-image-detail">
                        <div class="head">
                           <strong>Photo Details</strong> 
                        </div>
                        <ul>
                            <li> <span class="text">Date</span> <strong>:{{ $post->created_at->format('F d,Y') }}</strong> </li>
                            <li> <span class="text">Tag</span> <strong>: <a href="{{ url('tag/'. $post->slug_keyword .'.html') }}">{{ $post->keyword }}</a></strong> </li>
                            <li> <div class="">Source: {{ str_replace(".","[dot]",$post->url) }}</div> </li>
                         </ul>
                     </div>
                  </div>
               </div>
            </div>
            <div class="cp-tab-section">
               <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#tab1" role="tab" data-toggle="tab">Related Images</a></li>
               </ul>
               <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="tab1">
                     <div class="tab-carousel">
                        <?php $listing = $details; ?>
                        @include('photo.partials._listing')
                     </div>
                  </div>
               </div>
            </div>  
         </div>
      </div>
   </div>
</section>

@endsection