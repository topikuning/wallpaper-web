@extends('photo.home')

@section('content')
<section class="cp-inner-banner">
   <h1>{{ $page->title }}</h1>
   <ol class="breadcrumb">
      <li><a href="/">Home</a></li>
      <li><a href="#">Pages</a></li>
      <li class="active">{{ $page->title }}</li>
   </ol>
</section>
<div class="cp-main-content tb-50">
   <div class="cp-blog-posts">
      <div class="container">
         <div class="row">
            <div class="col-md-9">
               <ul class="cp-blog-list cp-post-details">
                  <li class="blog-post">
                     <h3>{{ $page->title }}</h3>
                     {!! $page->body !!}
                  </li>
               </ul>
            </div>
            <div class="col-md-3">
               <div class="sidebar">
                  <div class="widget search-widget">
                     <form class="navbar-form navbar-left" action="/search.html" method="get">
                        <div class="form-group">
                           <input type="text" class="form-control" placeholder="Search ..." name="q" required>
                        </div>
                        <button type="submit" class="search-btn"><i class="fa fa-search"></i></button>
                     </form>
                  </div>
                  <div class="widget tab-widget">
                     <h3>Images</h3>
                     <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Popular</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Recent</a></li>
                     </ul>
                     <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                           <ul class="popular-posts">
                            @foreach($popular as $row)
                              <li>
                                <?php $ext = pathinfo($row->image_url, PATHINFO_EXTENSION); ?>
                                 <div class="cp-thumb"><a href="{{ url($row->code . '-'.  $row->slug . '.html') }}"><img src="{{ url('assets/images/'. $row->slug . '-thumb.' .$ext) }}" alt="{{ $row->title }}"></a></div>
                                 <b>{{ $row->title }}</b> <span class="post-meta"><i>{{ $row->created_at }}</i></span>
                              </li>
                              @endforeach
                           </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                           <ul class="popular-posts">
                            @foreach($recent as $row)
                              <li>
                                <?php $ext = pathinfo($row->image_url, PATHINFO_EXTENSION); ?>
                                 <div class="cp-thumb"><a href="{{ url($row->code . '-'.  $row->slug . '.html') }}"><img src="{{ url('assets/images/'. $row->slug . '-thumb.' .$ext) }}" alt="{{ $row->title }}"></a></div>
                                 <b>{{ $row->title }}</b> <span class="post-meta"><i>{{ $row->created_at }}</i></span>
                              </li>
                              @endforeach
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="widget search-widget">
                     <h3>Categories</h3>
                     <ul class="categories">
                        @foreach($cat_photo as $row)
                        <li><a href="/category/{{ $row->slug }}.html">{{ $row->title }}</a></li>
                        @endforeach
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection