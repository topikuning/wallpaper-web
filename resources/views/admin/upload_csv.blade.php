<div class="row">
    <div class="col-md-12">
        <div>
            <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
            <div class="form-group">
                <label>Random minute</label>
                <input type="number" class="form-control" id="minutes" value="1440">
            </div>        
            
            <div class="form-group">
            <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Select files... (csv or txt file only)</span>
                <!-- The file input field used as target for the file upload widget -->
                <input id="fileupload" type="file" name="file" multiple>
            </span>
            <br>
            <br>
            <!-- The global progress bar -->
            <div id="progress" class="progress">
                <div class="progress-bar progress-bar-success"></div>
            </div>

            <div id="files">
                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

        $('#fileupload').fileupload({
            url: '/badmin/keywords/upload',
            acceptFileTypes: /(\.|\/)(csv|txt)$/i,
            dataType: 'json',
            autoUpload:true,
            done: function (e, data) {
                $('<div>').text(data.result.file +' was uploaded, refresh page to see uploaded records').appendTo('#files');
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }
        }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');


        $('#fileupload').bind('fileuploadsubmit', function (e, data) {
            // The example input, doesn't have to be part of the upload form:
            var input = $('#minutes');
            data.formData = {minutes: input.val()};
            if (!data.formData.minutes) {
              data.context.find('button').prop('disabled', false);
              input.focus();
              return false;
            }
        });

        $('#fileupload').bind('fileuploadprocessfail', function(e,data){
            BootstrapDialog.alert('<h4>File Extension is not valid</h4>');
        });
</script>