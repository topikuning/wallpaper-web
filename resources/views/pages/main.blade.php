@extends('home')

@section('content')

    <div class="wrapper">
      <div class="container">
          <div class="filters">
            <h1 class="sub-title">{{ config('site.main_title') }}</h1>
            <p>{{ config('site.main_body') }}</p>
          </div>

          <div class="row">
            <div class="main_container">
              <div class="fl-container span12">
                <div class="fullscreen_block">
                    <div class="fs_blog_module sorting_block" itemscope itemtype="http://schema.org/ImageGallery">
                      <?php $listing = $posts['data']; ?>
                      @include('partials._posts')
                    </div>
                    <ul class="pagerblock">
                         <?php $page = 1; $start=0; $limit= $posts['limit']; ?>
                         @while ($start < $posts['total'])

                         <?php
                            $url = url('/');
                            if($page > 1) $url = url('/page/'. $page);    
                         ?>

                         <li><a href="{{ $url }}" title="page {{ $page }}">{{ $page }}</a></li>
                         <?php $start = $start + $limit;  $page++; ?>
                         @endwhile
                      </ul>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div><!-- .wrapper -->


@endsection